﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MortgageDLL;

namespace MortgageForm {
    public partial class Form1 : Form {
        public Form1() {
            InitializeComponent();
        }

        private void btnCalc_Click(object sender, EventArgs e) {
            double doubOutput = MortgageDLL.MonthlyPayment.MortgagePay(double.Parse(txtInitial.Text), (double.Parse(txtRate.Text) * 0.01 /12 ), (int.Parse(txtTerm.Text) * 12));
            ;
            txtPay.Text = doubOutput.ToString("C2");
        }
    }
}
